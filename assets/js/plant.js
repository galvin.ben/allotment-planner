export default {
  parse (newPlant) {
    console.log('planting')
    let parsedPlant = {}
    parsedPlant['month'] = parseInt(newPlant.date.split('-')[1])
    parsedPlant['date'] = parseInt(newPlant.date.split('-')[2])
    parsedPlant['year'] = parseInt(newPlant.date.split('-')[0])
    parsedPlant['info'] = newPlant.info
    parsedPlant['name'] = newPlant.name
    parsedPlant['harvest'] = []
    for (var i = +newPlant.harvestStartDate.split('-')[1]; i <= newPlant.harvestEndDate.split('-')[1]; i++) {
      parsedPlant['harvest'].push(i)
    }
    return parsedPlant
  },
  save (app, parsedPlant) {
    app.$firebase.app().database().ref('plants/' + parsedPlant.name).set(parsedPlant)
  },
  async getMonthPlants (app) {
    var ref = app.$firebase.app().database().ref()
    var plants = await ref.once('value')
      .then(function (snap) {
        return snap.val().plants
      })
    var monthPlants = []
    for (var plant in plants) {
      if (plants[plant].month === parseInt(app.month)) {
        monthPlants.push(plants[plant])
      }
    }
    return monthPlants
  },
  async getHarvestPlants (app) {
    var ref = app.$firebase.app().database().ref()
    var plants = await ref.once('value')
      .then(function (snap) {
        return snap.val().plants
      })
    var monthPlants = []
    for (var plant in plants) {
      for (var harvestMonth in plants[plant].harvest) {
        if (plants[plant].harvest[harvestMonth] === parseInt(app.month)) {
          monthPlants.push(plants[plant])
          break
        }
      }
    }
    return monthPlants
  }
}
