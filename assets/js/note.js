export default {
  parse (newNote) {
    let parsedNote = {}
    parsedNote['title'] = newNote.title
    parsedNote['text'] = newNote.text
    parsedNote['month'] = parseInt(newNote.month.split('-')[1])
    return parsedNote
  },
  save (app, parsedNote) {
    app.$firebase.app().database().ref('notes/' + Math.floor(Date.now() / 1000)).set(parsedNote)
  },
  async getNotes (app) {
    var ref = app.$firebase.app().database().ref()
    var notes = await ref.once('value')
      .then(function (snap) {
        return snap.val().notes
      })
    var relevantNotes = []
    for (var note in notes) {
      if (notes[note].month === parseInt(app.month) || notes[note].month === '') {
        relevantNotes.push(notes[note])
      }
    }
    return relevantNotes
  }
}
