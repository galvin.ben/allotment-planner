export default {
  parse (newTodo) {
    let parsedTodo = {}
    parsedTodo['text'] = newTodo.text
    parsedTodo['month'] = parseInt(newTodo.month.split('-')[1])
    parsedTodo['done'] = false
    parsedTodo['timestamp'] = Math.floor(Date.now() / 1000)
    return parsedTodo
  },
  save (app, parsedTodo) {
    app.$firebase.app().database().ref('todos/' + parsedTodo.timestamp).set(parsedTodo)
  },
  update (app, timestamp, done) {
    app.$firebase.app().database().ref('todos/' + timestamp + '/done').set(done)
  },
  async getTodos (app) {
    var ref = app.$firebase.app().database().ref()
    var todos = await ref.once('value')
      .then(function (snap) {
        return snap.val().todos
      })
    var relevantTodos = []
    for (var todo in todos) {
      if (todos[todo].month === parseInt(app.month) || todos[todo].month === '') {
        relevantTodos.push(todos[todo])
      }
    }
    return relevantTodos
  }
}
