# allotment-planner

> Nuxt.js + Vuetify.js project using Firebase for the database, functions and backend.

Allotment Planner is a web app designed to track when things are planted at the 
allotment, when they will be ready to harvest, notes, and todos for each month.

Currently at a basic working level, design needs a lot of focus as it's not the prettiest of apps.

![](images/image.png)

[Demo](https://allotment-planner.firebaseapp.com/)

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [Nuxt.js](https://github.com/nuxt/nuxt.js) and [Vuetify.js](https://vuetifyjs.com/) documentation.
