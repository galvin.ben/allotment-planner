export default function ({app, redirect}) {
  if (!window.location.pathname.includes('login')) {
    app.$firebase.auth().onAuthStateChanged(function (user) {
      if (!user) {
        window.location.replace('/login')
      }
    }, function (error) {
      console.log(error)
    })
  }
}
