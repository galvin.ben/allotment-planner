import firebase from 'firebase'
// import firebaseui from 'firebaseui'

const config = {
  apiKey: 'AIzaSyBqcXQojsHoohIBpauHBamMGhYmTgH0Bwc',
  authDomain: 'allotment-planner.firebaseapp.com',
  databaseURL: 'https://allotment-planner.firebaseio.com',
  projectId: 'allotment-planner',
  storageBucket: 'allotment-planner.appspot.com',
  messagingSenderId: '961616103275'
}

firebase.initializeApp(config)

var database = firebase.database()

export default (app, inject) => {
  inject('firebase', firebase)
  inject('firebasedb', database)
}
